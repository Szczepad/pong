#include <string>
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
//
// Created by dennis on 02.11.18.
//

//Texture wrapper header

#ifndef PONG_TEXTURE_H
#define PONG_TEXTURE_H
class Texture {
public:
    //Constructor and destructor
    //Constructor for normal images etc.
    Texture(SDL_Renderer* gRenderer);
    //Constructor for text
    Texture(SDL_Renderer* gRenderer, TTF_Font* gFont);
    ~Texture();

    //Load image from Path
    bool loadFromFile(std::string path);

    //Deallocate texture
    void free();

    //Render texture at given point
    void render(int x, int y);

    //Get image dimensions
    int getWidth();
    int getHeight();

    //Create image from font string
    bool loadFromRenderedText(std::string textureText, SDL_Color textColor);

private:
    //The actual texture
    SDL_Texture* mTexture;

    //Image dimensions
    int mWidth;
    int mHeight;
    //Renderer and Font
    SDL_Renderer* mRenderer;
    TTF_Font* mFont = NULL;
};


#endif //PONG_TEXTURE_H
