//
// Created by dennis on 03.11.18.
//
#include <SDL2/SDL.h>
#include <strings.h>

#ifndef PONG_PADDLE_H
#define PONG_PADDLE_H


class Paddle {
public:
    //Construct paddle with position
    Paddle(SDL_Renderer *gRenderer, int x, int y, int w, int h, bool player);

    // Destroctor
    ~Paddle();

    //Moves the paddle and reacts on collisions
    void move(SDL_Rect &collider);

    //Renders the paddle
    void render();

    //Handles input
    void handleEvent(SDL_Event &e, SDL_Rect &ballCollider);

    //Gets collision box
    SDL_Rect &getCollider();

    int getWidth();

    int getPoints();

    // Add a point
    void addPoint();

private:
    //Global renderer as member
    SDL_Renderer *mRenderer;
    //Dimensions
    int x;
    int y;
    int w;
    int h;
    int velocity = 0;
    int maxVelocity = 1;
    //Points of the paddle
    int points = 0;
    //Collision Box
    SDL_Rect mCollider;
    //Player flag
    bool player;
};


#endif //PONG_PADDLE_H
