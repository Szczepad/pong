//
// Created by dennis on 06.11.18.
//
#include <SDL2/SDL.h>

#ifndef PONG_TIMER_H
#define PONG_TIMER_H


class Timer {
public:
    //Constructor
    Timer();

    //Timer actions
    void start();
    void stop();
    void pause();
    void unpause();

    //Get Time
    Uint32 getTicks();

    //Status of the timer
    bool isStarted();
    bool isPaused();
private :
    //Initial ticks when the timer ist started
    Uint32 mStartTicks;
    //Buffers ticks for pause
    Uint32 mPausedTicks;
    //Timer Status
    Uint32 mPaused;
    Uint32 mStarted;

};


#endif //PONG_TIMER_H
