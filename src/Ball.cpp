//
// Created by dennis on 04.11.18.
//

#include "Ball.h"
#include <stdlib.h>
#include <time.h>


Ball::Ball(SDL_Renderer *gRenderer, int x, int y) {
    //Set renderer
    this->renderer = gRenderer;
    //Set dimensions
    this->x = x;
    this->y = y;
    this->w = 10;
    this->h = 10;
    this->xVelocity = 0;
    this->yVelocity = 0;

    //Set collision box
    this->mCollider.x = x;
    this->mCollider.y = y;
    this->mCollider.h = h;
    this->mCollider.w = w;

}

void Ball::start() {
    //Let the ball move in a random direction
    //Initialize random seed
    srand(time(NULL));
    //Should the ball go to the enemy or the player?
    while (yVelocity == 0) {
        this->yVelocity = (rand() % 3) - 2;
    }
    //In which direction should the ball go?
    while (xVelocity < 0.2 && xVelocity > -0.2) {
        this->xVelocity = (rand() % 200) - 100;
    }
    //convert the number to a float
    this->xVelocity /= 100;
}

Ball::~Ball() {
    renderer = NULL;
}

void Ball::render() {
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderFillRect(renderer, &mCollider);
}

bool Ball::checkCollision(SDL_Rect a, SDL_Rect b) {
    //Dimensions of the boxes
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Set dimensions of box A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.w;

    //Set dimensions of box B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;

    //Check for collision
    if (bottomA <= topB) {
        return false;
    }
    if (topA >= bottomB) {
        return false;
    }
    if (rightA <= leftB) {
        return false;
    }
    if (leftA >= rightB) {
        return false;
    }
    return true;
}

void Ball::move(SDL_Rect &playerPaddle, SDL_Rect &enemyPaddle) {
    //Move the Ball
    x += xVelocity;
    y += yVelocity;
    //Adjust the collider
    mCollider.x = x;
    mCollider.y = y;

    //If the Ball touched a wall, move back
    if (x < 0 || (x + w > 800)) {
        //Move back
        xVelocity *= (-1);
        mCollider.x = x;
    }
    //Check if ball was hit by a paddle
    if (checkCollision(mCollider, playerPaddle) || checkCollision(mCollider, enemyPaddle)) {
        this->hit();
    }
}

void Ball::hit() {
    //Speed up the ball;
    if (xVelocity > 0.1) {
        xVelocity++;
    }
    //Shift the direction of the ball
    yVelocity *= (-1);
}

SDL_Rect &Ball::getCollider() {
    return mCollider;
}

int Ball::getY() {
    return y;
}

void Ball::reset(int nx, int ny) {
    //Gives the ball a new position
    this->x = nx;
    this->y = ny;

    mCollider.x = x;
    mCollider.y = y;

    start();
}