#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <sstream>
#include "Texture.h"
#include "Paddle.h"
#include "Ball.h"
#include "Timer.h"

//Screen dimension
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int SCREEN_FPS = 60;
const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;
//The window that SDL will render to
SDL_Window *gWindow = NULL;

//The renderer that SDL will be using
SDL_Renderer *gRenderer = NULL;

//Globally used font
TTF_Font *gFont = NULL;


//Initialize SDL and create window
bool init();

//Shut down SDL
bool close();

//Loads media
bool loadMedia();

enum gamestate {
    MENU,
    PAUSE,
    GAME
};

bool init() {
    //Success flag
    bool success = true;
    //Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Couldn't initialize SDL! SDL_Error : %s\n", SDL_GetError());
        success = false;
    } else {
        //Create Window
        gWindow = SDL_CreateWindow("Pong", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                                   SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (gWindow == NULL) {
            printf("Window could not be created! SDL_Error : %s\n", SDL_GetError());
            success = false;
        } else {
            //Create renderer for Window
            gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (gRenderer == NULL) {
                printf("Renderer could not be created! SDL_ERROR: %s\n", SDL_GetError());
                success = false;
            } else {
                //Initialize renderer color
                SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if (!(IMG_Init(imgFlags) & imgFlags)) {
                    printf("SDL_image could not initialize! SDL_image Error : %s\n", IMG_GetError());
                    success = false;
                }
                //Initialize SDL TTF
                if (TTF_Init() == -1) {
                    printf("SDL_ttf could not initialize! SDL_ttf Error : %s \n", TTF_GetError());
                    success = false;
                }
            }
        }
    }
    return success;
}

bool loadMedia() {
    //Loading success flag
    bool success = true;

    //Open the font
    gFont = TTF_OpenFont("src/Raleway-Black.ttf", 28);
    if (gFont == NULL) {
        printf("Failed to load font! SDL_ttf Error: %s \n", TTF_GetError());
        success = false;
    }
    return success;
}

bool close() {
    //Free global font
    TTF_CloseFont(gFont);
    gFont = NULL;

    //Destroy window
    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;
    gRenderer = NULL;

    //Quit SDL
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();

}

int main(int argc, char *args[]) {
    if (!init()) {
        printf("Failed to initialize!\n");
    } else {
        if (!loadMedia()) {
            printf("Failed to load media!\n");
        } else { //Start Game
            //Main loop flag
            bool quit = false;
            //gameState flag
            int gameState = MENU;
            //Event handler
            SDL_Event e;

            //FPS timer
            Timer fpsTimer;
            //FPS Cap timer
            Timer capTimer;

            //Start counting FPS
            int countedFrames = 0;
            fpsTimer.start();

            //Paddles for Player and Enemy
            Paddle playerPaddle(gRenderer, SCREEN_WIDTH / 2, SCREEN_HEIGHT - 11, 100, 10, 1);
            Paddle enemyPaddle(gRenderer, SCREEN_WIDTH / 2, 1, 100, 10, 0);
            Ball ball(gRenderer, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
            //Initialize the Ball direction
            ball.start();

            //Initialize the Points-txt
            Texture playerPointsTexture(gRenderer, gFont);
            Texture enemyPointsTexture(gRenderer, gFont);
            //Main loop
            while (!quit) {
                //Start cap timer
                capTimer.start();
                //Poll Events
                while (SDL_PollEvent(&e) != 0) {
                    if (e.type == SDL_QUIT) {
                        quit = true;
                    }
                    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE && gameState == GAME) {
                        gameState = PAUSE;
                    }
                }

                //Menu Screen
                if (gameState == MENU) {
                    //Render Menu Screen
                    SDL_Color textColor = {255, 255, 255};
                    Texture textTexture(gRenderer, gFont);
                    textTexture.loadFromRenderedText("Press ENTER to start", textColor);
                    //Show menu Screen
                    SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
                    SDL_RenderClear(gRenderer);
                    //Render current frame
                    textTexture.render((SCREEN_WIDTH - textTexture.getWidth()) / 2,
                                       (SCREEN_HEIGHT - textTexture.getHeight()) / 2);
                    //Update screen
                    SDL_RenderPresent(gRenderer);

                    //Check if enter was pressed
                    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN) {
                        //Clear the Screen and get into the game
                        textTexture.free();
                        SDL_RenderClear(gRenderer);
                        SDL_RenderPresent(gRenderer);
                        gameState = GAME;
                    }
                } else if (gameState == GAME) {
                    //Check if there's a point.
                    if (ball.getY() <= 0) {
                        playerPaddle.addPoint();
                        ball.reset(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
                    } else if (ball.getY() >= SCREEN_HEIGHT) {
                        enemyPaddle.addPoint();
                        ball.reset(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
                    }

                    //Handle events
                    playerPaddle.handleEvent(e, ball.getCollider());
                    enemyPaddle.handleEvent(e, ball.getCollider());
                    //TODO Event for ball

                    //Move Objects
                    playerPaddle.move(ball.getCollider());
                    enemyPaddle.move(ball.getCollider());
                    ball.move(playerPaddle.getCollider(), enemyPaddle.getCollider());

                    //Clear screen
                    SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
                    SDL_RenderClear(gRenderer);

                    //Render objects
                    playerPaddle.render();
                    enemyPaddle.render();
                    ball.render();
                    //Render Text
                    SDL_Color textColor = {255, 255, 255};
                    //Create strings from Points
                    std::stringstream playerPointsString;
                    std::stringstream enemyPointsString;
                    playerPointsString << playerPaddle.getPoints();
                    enemyPointsString << enemyPaddle.getPoints();

                    playerPointsTexture.loadFromRenderedText(playerPointsString.str().c_str(), textColor);
                    enemyPointsTexture.loadFromRenderedText(enemyPointsString.str().c_str(), textColor);

                    playerPointsTexture.render(playerPointsTexture.getWidth() / 2,
                                               ((SCREEN_HEIGHT - playerPointsTexture.getHeight()) / 2) + 50);
                    enemyPointsTexture.render(SCREEN_WIDTH - playerPointsTexture.getWidth(),
                                              ((SCREEN_HEIGHT - playerPointsTexture.getHeight()) / 2) - 50);

                    //Update Screen
                    SDL_RenderPresent(gRenderer);
                    ++countedFrames;

                    int frameTicks = capTimer.getTicks();
                    if (frameTicks < SCREEN_TICKS_PER_FRAME) {
                        SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
                    }
                } else if (gameState == PAUSE) {
                    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN) {
                        gameState = GAME;
                    }
                }
            }
        }
        //End of Program. Close SDL
        close();

        return 0;
    }
}