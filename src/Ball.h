//
// Created by dennis on 04.11.18.
//
#include <SDL2/SDL.h>

#ifndef PONG_BALL_H
#define PONG_BALL_H


class Ball {
public:
    //Constructor and destructor
    Ball(SDL_Renderer *gRenderer, int x, int y);

    ~Ball();

    //Initialize the game
    void start();

    //Movement and render
    void move(SDL_Rect &playerPaddle, SDL_Rect &enemyPaddle);

    void render();

    //Collision with paddle
    bool checkCollision(SDL_Rect a, SDL_Rect b);

    void hit();

    //Reset the ball after a point
    void reset(int nx, int ny);

    //Getter
    SDL_Rect &getCollider();

    int getY();


private :
    //global Renderer as Member
    SDL_Renderer *renderer;
    //Dimensions
    double x;
    double y;
    int w;
    int h;
    double xVelocity;
    double yVelocity;
    //Collision box;
    SDL_Rect mCollider;
};


#endif //PONG_BALL_H
