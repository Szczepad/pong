//
// Created by dennis on 06.11.18.
//

#include "Timer.h"

Timer::Timer(){
    mStartTicks=0;
    mPausedTicks=0;

    mPaused=false;
    mStarted=false;
}

void Timer::start(){
    //Start the timer
    mStarted = true;
    //Unpause the timer
    mPaused = false;

    //Get the current time
    mStartTicks = SDL_GetTicks();
    mPausedTicks=0;
}

void Timer::stop() {
    //Stop the timer
    mStarted = false;
    //Unpause the timer
    mPaused = false;

    //Clear Ticks
    mStartTicks = 0;
    mPausedTicks = 0;

}

void Timer::pause() {
    //If timer has started and isn't paused
    if(mStarted && !mPaused) {
        mPaused = true;

        //Calculate the paused ticks
        mPausedTicks = SDL_GetTicks() - mStartTicks;
        mStartTicks = 0;
    }
}

void Timer::unpause() {
    //If the timer has started and is paused
    if(mStarted && mPaused) {
        mPaused = false;

        //Reset the starting ticks
        mStartTicks = SDL_GetTicks() - mPausedTicks;

        //Reset the paused ticks
        mPausedTicks = 0;
    }
}

Uint32 Timer::getTicks() {
    //The actual timer time
    Uint32 time = 0;

    if(mStarted) {
        //If timer is paused
        if(mPaused) {
            time = mPausedTicks;
        }
        else {
            time = SDL_GetTicks() - mStartTicks;
        }
    }
    return time;
}

bool Timer::isStarted() {
    return mStarted;
}

bool Timer::isPaused() {
    //Timer is running and paused
    return mPaused && mStarted;
}