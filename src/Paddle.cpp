//
// Created by dennis on 03.11.18.
//

#include <SDL2/SDL.h>
#include "Paddle.h"
#include "Ball.h"


Paddle::Paddle(SDL_Renderer *gRenderer, int x, int y, int w, int h, bool player) {
    //Set renderer
    mRenderer = gRenderer;

    //Set dimensions
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    this->velocity = 0;

    //Set dimensions of collision box
    mCollider.x = x;
    mCollider.y = y;
    mCollider.w = w;
    mCollider.h = h;

    this->player = player;

}

Paddle::~Paddle() {
    mRenderer = NULL;
}

void Paddle::handleEvent(SDL_Event &e, SDL_Rect &ballCollider) {
    //Handle events for player
    if (this->player == true) {
        //If a key was pressed
        if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
            //Change the velocity
            switch (e.key.keysym.sym) {
                case SDLK_LEFT :
                    velocity -= maxVelocity;
                    break;
                case SDLK_RIGHT :
                    velocity += maxVelocity;
                    break;
            }
        }
            //If a key was released
        else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
            //Change the velocity
            switch (e.key.keysym.sym) {
                case SDLK_LEFT:
                    velocity = 0;
                    break;
                case SDLK_RIGHT:
                    velocity = 0;
                    break;
            }
        }
    } else {
        if (ballCollider.x > (this->x + (this->w - 10))) {
            velocity += maxVelocity;
        } else if (ballCollider.x < (this->x + 10)) {
            velocity -= maxVelocity;
        } else {
            velocity = 0;
        }
    }
}

void Paddle::move(SDL_Rect &collider) {
    x += velocity;
    mCollider.x = x;

    //If the paddle went too far to the left or right
    if (x <= 0) {
        //Move back
        x = 0;
        mCollider.x = x;
    } else if ((x + w >= 800)) {
        x = 800 - w;
        mCollider.x = x;
    }
}

void Paddle::render() {
    SDL_SetRenderDrawColor(mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderFillRect(mRenderer, &mCollider);
}

int Paddle::getWidth() {
    return w;
}

SDL_Rect &Paddle::getCollider() {
    return mCollider;
}

void Paddle::addPoint() {
    points++;
}

int Paddle::getPoints() {
    return points;
}