#include "Texture.h"
#include <SDL_render.h>
#include <SDL_image.h>
//
// Created by dennis on 02.11.18.
//

//Texture Wrapper cpp

//Constructor
Texture::Texture(SDL_Renderer* gRenderer)
{
    mTexture = NULL;
    mWidth = 0;
    mHeight = 0;
    mRenderer = gRenderer;
}

Texture::Texture(SDL_Renderer* gRenderer, TTF_Font* gFont) {
    mTexture= NULL;
    mWidth = 0;
    mHeight= 0;
    mRenderer = gRenderer;
    mFont = gFont;
}

//Destructor
Texture::~Texture()
{
    free();
}

//Load texture from File
bool Texture::loadFromFile(std::string path)
{
    //Free preexisting texture
    free();
    //The final texture
    SDL_Texture* newTexture = NULL;
    //Load image from file
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if(loadedSurface == NULL) {
        printf("Unable to load image %s! SDL_image Error: %s\n",path.c_str(),IMG_GetError());
    }
    else
    {
     //Create texture form Surface
     newTexture = SDL_CreateTextureFromSurface(mRenderer,loadedSurface);
     if(newTexture == NULL)
     {
         printf("Unable to create texture from %s! SDL Error: %s\n",path.c_str(),SDL_GetError());
     }
     else
     {
         //Get image dimensions
         mWidth = loadedSurface -> w;
         mHeight = loadedSurface -> h;
     }
     //Delete the placeholder surface
     SDL_FreeSurface(loadedSurface);
    }
    //Assign the texture
    mTexture=newTexture;
    //Return success
    return mTexture!= NULL;
}

void Texture::free()
{
    //Free texture if it exists
    if(mTexture != NULL) {
        SDL_DestroyTexture(mTexture);
        mTexture = NULL;
        mWidth = 0;
        mHeight = 0;
    }
}

void Texture::render(int x, int y) {
    //Set rendering space and render to screen
    SDL_Rect renderQuad = {x,y,mWidth,mHeight};
    SDL_RenderCopy(mRenderer,mTexture,NULL,&renderQuad);
}

int Texture::getWidth()
{
    return mWidth;
}

int Texture::getHeight()
{
    return mHeight;
}

bool Texture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
    //Delete preexisting texture
    free();
    //Render text surface
    SDL_Surface* textSurface = TTF_RenderText_Solid(mFont, textureText.c_str(),textColor);
    if(textSurface == NULL)
    {
        printf("Unable to render text surface! SDL_ttf Error: %s\n",TTF_GetError());
    }
    else
    {

        //Create texture from surface
        mTexture = SDL_CreateTextureFromSurface(mRenderer,textSurface);
        if(mTexture==NULL)
        {
            printf("Unable to create texture from rendered text! SDL Error: %s \n",SDL_GetError());
        }
        else
        {
            //Get image dimensions
            mWidth = textSurface->w;
            mHeight = textSurface->h;
        }
        //Delete old surface
        SDL_FreeSurface(textSurface);
    }
    //Return success
    return mTexture != NULL;
}